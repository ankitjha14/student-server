package com.studentlist.studentlist.controllers;

import java.util.List;

import com.studentlist.studentlist.models.Student;
import com.studentlist.studentlist.services.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RestController
public class StudentController {

    @Autowired
    StudentService studentService;

    @GetMapping("/students")
    List<Student> getStudents(String[] args) {
        return studentService.fetchStudents();
    }

    @PostMapping("/students")
    Student addStudent(@RequestBody Student student) {
        return studentService.addStudent(student);
    }

    @PutMapping("/students/{id}")
    Student updateStudent(@RequestBody Student student, @PathVariable int id) {
        return studentService.updateStudent(student, id);
    }

    @GetMapping("/students/{id}")
    Student getStudentDetails(@PathVariable int id) {
        return studentService.fetchStudentDetails(id);
    }

    @DeleteMapping("/students/{id}")
    String deleteStudent(@PathVariable int id) {
        studentService.deleteStudent(id);
        return "successfully deleted!!";
    }

}
