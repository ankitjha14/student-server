package com.studentlist.studentlist.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.studentlist.studentlist.models.Student;
import com.studentlist.studentlist.repositories.StudentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    public List<Student> fetchStudents() {
        List<Student> students = new ArrayList<>();
        studentRepository.findAll().forEach(student -> students.add(student));
        return students;
    }

    public Student fetchStudentDetails(int id) {
        Optional<Student> student = studentRepository.findById(id);
        if(student.isPresent()){
            return student.get();
        } else {
            throw new Error("student not found!!");
        }
    }

    public Student addStudent(Student student) {
        return studentRepository.save(student);
    }

    public Student updateStudent(Student student, int id) {
        if (isStudentExist(id)) {
            student.setId(id);
            return studentRepository.save(student);
        } else {
            throw new Error("student not exist");
        }
    }

    public void deleteStudent(int id) {
        if (isStudentExist(id)) {
            studentRepository.deleteById(id);
        } else {
            throw new Error("student not exist");
        }
    }

    private Boolean isStudentExist(int id) {
        Boolean isStudentExist = studentRepository.existsById(id);
        return isStudentExist;
    }
}