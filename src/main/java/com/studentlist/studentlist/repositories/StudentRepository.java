package com.studentlist.studentlist.repositories;

import com.studentlist.studentlist.models.Student;

import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, Integer> {
}